#include "stdafx.h"
#include "Graphics/GraphicsContext.h"

#include <psp2/gxm.h>
#include <psp2/display.h>

#include "gxm.h"
#include "math_utils.h"

#include "Config/ConfigOptions.h"
#include "Core/ROM.h"
#include "Debug/DBGConsole.h"
#include "Debug/Dump.h"
#include "Graphics/ColourValue.h"
#include "Graphics/PngUtil.h"
#include "SysPSP/Graphics/VideoMemoryManager.h"
#include "Utility/IO.h"
#include "Utility/Preferences.h"
#include "Utility/Profiler.h"
#include "Utility/VolatileMem.h"

extern void HandleEndOfFrame();

#define SCR_WIDTH 960
#define SCR_HEIGHT 544

class IGraphicsContext : public CGraphicsContext
{
public:
	IGraphicsContext();
	virtual ~IGraphicsContext();

	bool				Initialise();
	bool				IsInitialised() const { return mInitialised; }

	void				SwitchToChosenDisplay();
	void				SwitchToLcdDisplay();
	void				StoreSaveScreenData();

	void				ClearAllSurfaces();

	void				ClearToBlack();
	void				ClearZBuffer();
	void				ClearColBuffer(const c32 &colour);
	void				ClearColBufferAndDepth(const c32 &colour);

	void				BeginFrame();
	void				EndFrame();
	void				UpdateFrame(bool wait_for_vbl);
	void				GetScreenSize(u32 * width, u32 * height) const;

	void				SetDebugScreenTarget( ETargetSurface buffer );

	void				ViewportType(u32 *d_width, u32 *d_height) const;
	void				DumpScreenShot();
	void				DumpNextScreen()			{ mDumpNextScreen = 2; }

private:
	void				SaveScreenshot(const char* filename, s32 x, s32 y, u32 width, u32 height);

private:
	bool				mInitialised;

	u32					mDumpNextScreen;
};

//*************************************************************************************
//
//*************************************************************************************
template<> bool CSingleton< CGraphicsContext >::Create()
{
#ifdef DAEDALUS_ENABLE_ASSERTS
	DAEDALUS_ASSERT_Q(mpInstance == nullptr);
#endif
	mpInstance = new IGraphicsContext();
	return mpInstance->Initialise();
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

IGraphicsContext::IGraphicsContext()
	:	mInitialised(false)
	,	mDumpNextScreen(false)
{
	LOG("IGraphicsContext::IGraphicsContext()\n");

	gxm_init();
}

IGraphicsContext::~IGraphicsContext()
{
	gxm_finish();
}


bool IGraphicsContext::Initialise()
{
	LOG("IGraphicsContext::Initialise\n");
	mInitialised = true;
	return true;
}

void IGraphicsContext::ClearAllSurfaces()
{
}

void IGraphicsContext::ClearToBlack()
{
	LOG("IGraphicsContext::ClearToBlack\n");

	static const float clear_color[4] = {
		0.0f, 0.0f, 0.0f, 1.0f
	};

	gxm_clear(clear_color, true, false);
}

void IGraphicsContext::ClearZBuffer()
{
	LOG("IGraphicsContext::ClearZBuffer\n");

	static const float clear_color[4] = {
		0.0f, 0.0f, 0.0f, 1.0f
	};

	gxm_clear(clear_color, false, true);
}

void IGraphicsContext::ClearColBuffer(const c32 & colour)
{
	LOG("IGraphicsContext::ClearColBuffer\n");

	static const float clear_color[4] = {
		colour.GetRf(), colour.GetGf(), colour.GetBf(), colour.GetAf()
	};

	gxm_clear(clear_color, true, false);
}

void IGraphicsContext::ClearColBufferAndDepth(const c32 & colour)
{
	LOG("IGraphicsContext::ClearColBufferAndDepth\n");

	static const float clear_color[4] = {
		colour.GetRf(), colour.GetGf(), colour.GetBf(), colour.GetAf()
	};

	gxm_clear(clear_color, true, true);
}

void IGraphicsContext::BeginFrame()
{
	LOG("IGraphicsContext::BeginFrame\n");
	gxm_begin_frame();
}

void IGraphicsContext::EndFrame()
{
	LOG("IGraphicsContext::EndFrame\n");
	gxm_end_frame();
	gxm_vertex_buffer_color_reset();
	gxm_vertex_buffer_texture_reset();

	HandleEndOfFrame();
}

void IGraphicsContext::UpdateFrame(bool wait_for_vbl)
{
	LOG("IGraphicsContext::UpdateFrame\n");
	DAEDALUS_PROFILE( "IGraphicsContext::UpdateFrame" );
}

void IGraphicsContext::SetDebugScreenTarget(ETargetSurface buffer)
{

}

void IGraphicsContext::ViewportType(u32 *d_width, u32 *d_height) const
{
	*d_width = SCR_WIDTH;
	*d_height = SCR_HEIGHT;
}

void IGraphicsContext::SaveScreenshot(const char* filename, s32 x, s32 y, u32 width, u32 height)
{
}

void IGraphicsContext::DumpScreenShot()
{
}

void IGraphicsContext::StoreSaveScreenData()
{
}

void IGraphicsContext::GetScreenSize(u32 * p_width, u32 * p_height) const
{
	*p_width = SCR_WIDTH;
	*p_height = SCR_HEIGHT;
}
