#pragma once

#include <cinttypes>
#include <psp2/gxm.h>
#include "math_utils.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

struct color_vertex {
	vector3f position;
	uint32_t color;
} __attribute__((packed));

struct texture_vertex {
	vector3f position;
	uint32_t color;
	float u, v;
} __attribute__((packed));

struct gxm_texture {
	SceGxmTexture texture;
	SceUID data_uid;
	uint32_t stride;
};

int gxm_init();
int gxm_finish();
void gxm_clear(const float color[4], bool clear_color, bool clear_depth);
void gxm_begin_frame();
void gxm_end_frame();

void gxm_set_color_program();
void gxm_set_texture_program();

void gxm_create_texture(struct gxm_texture *texture, uint32_t width, uint32_t height, SceGxmTextureFormat format);
void gxm_texture_set_fragment_texture(const struct gxm_texture *texture, uint32_t texture_index);
void *gxm_texture_get_data(const struct gxm_texture *texture);
uint32_t gxm_texture_get_width(const struct gxm_texture *texture);
uint32_t gxm_texture_get_height(const struct gxm_texture *texture);
uint32_t gxm_texture_get_stride(const struct gxm_texture *texture);
void gxm_texture_set_min_filter(struct gxm_texture *texture, SceGxmTextureFilter filter);
void gxm_texture_set_mag_filter(struct gxm_texture *texture, SceGxmTextureFilter filter);
void gxm_texture_set_mip_filter(struct gxm_texture *texture, SceGxmTextureMipFilter filter);
void gxm_texture_set_u_addr_mode(struct gxm_texture *texture, SceGxmTextureAddrMode mode);
void gxm_texture_set_v_addr_mode(struct gxm_texture *texture, SceGxmTextureAddrMode mode);
void gxm_free_texture(struct gxm_texture *texture);

void gxm_vertex_buffer_color_push(SceGxmPrimitiveType prim, const struct color_vertex *vertices, uint32_t count);
void gxm_vertex_buffer_texture_push(SceGxmPrimitiveType prim, const struct texture_vertex *vertices, uint32_t count);
void gxm_vertex_buffer_color_reset();
void gxm_vertex_buffer_texture_reset();

void gxm_set_viewport(uint32_t offset_x, uint32_t offset_y, int32_t width, int32_t height, float z_offset, float z_scale);
void gxm_set_polygon_mode(SceGxmPolygonMode mode);
void gxm_set_fragment_program_enable(SceGxmFragmentProgramMode mode);
void gxm_set_depth_write_mode(SceGxmDepthWriteMode mode);
void gxm_set_depth_test_function(SceGxmDepthFunc func);

void gxm_reserve_color_vertex_program_transform_param_default_uniform_data(uint32_t component_count, const float *data);
