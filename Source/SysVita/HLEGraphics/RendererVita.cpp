#include "stdafx.h"
#include "RendererVita.h"

#include <psp2/gxm.h>
#include "SysVita/Graphics/gxm.h"

//#include "Combiner/BlendConstant.h"
//#include "Combiner/CombinerTree.h"
//#include "Combiner/RenderSettings.h"
#include "Core/ROM.h"
#include "Debug/Dump.h"
#include "Graphics/GraphicsContext.h"
#include "Graphics/NativeTexture.h"
#include "HLEGraphics/CachedTexture.h"
#include "HLEGraphics/DLDebug.h"
#include "HLEGraphics/RDPStateManager.h"
#include "HLEGraphics/TextureCache.h"
#include "Math/MathUtil.h"
#include "OSHLE/ultra_gbi.h"
#include "Utility/IO.h"
#include "Utility/Profiler.h"

BaseRenderer *gRenderer    = nullptr;
RendererVita  *gRendererVita = nullptr;

static ScePspFMatrix4 gProjection;

void sceGuSetMatrix(EGuMatrixType type, const ScePspFMatrix4 *mtx)
{
	if (type == GU_PROJECTION) {
		//LOG("Setting Projection matrix:\n");
		matrix4x4_transpose((float (*)[4])&gProjection, (float (*)[4])mtx);
		//memcpy(&gProjection, mtx, sizeof(gProjection));
		/*for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				LOG("%.8f ", gProjection.m[i * 4 + j]);
			}
			LOG("\n");
		}*/
	}
}

RendererVita::RendererVita()
{
}

RendererVita::~RendererVita()
{
}

void RendererVita::RestoreRenderStates()
{
	gxm_set_color_program();

	gxm_set_fragment_program_enable(SCE_GXM_FRAGMENT_PROGRAM_ENABLED);

	gxm_set_depth_write_mode(SCE_GXM_DEPTH_WRITE_DISABLED);
	gxm_set_depth_test_function(SCE_GXM_DEPTH_FUNC_ALWAYS);
}

void RendererVita::PrepareRenderState(const float (&mat_project)[16], bool disable_zbuffer)
{
	DAEDALUS_PROFILE( "RendererVita::PrepareRenderState" );

	gxm_set_color_program();

	gxm_set_fragment_program_enable(SCE_GXM_FRAGMENT_PROGRAM_ENABLED);

	if (disable_zbuffer) {
		gxm_set_depth_write_mode(SCE_GXM_DEPTH_WRITE_DISABLED);
		gxm_set_depth_test_function(SCE_GXM_DEPTH_FUNC_ALWAYS);
	} else {
		if ((mTnL.Flags.Zbuffer & gRDPOtherMode.z_cmp) | gRDPOtherMode.z_upd)
			gxm_set_depth_test_function(SCE_GXM_DEPTH_FUNC_GREATER_EQUAL);
		else
			gxm_set_depth_test_function(SCE_GXM_DEPTH_FUNC_ALWAYS);

		gxm_set_depth_write_mode(gRDPOtherMode.z_upd ?
			SCE_GXM_DEPTH_WRITE_ENABLED :
			SCE_GXM_DEPTH_WRITE_DISABLED);
	}

	gxm_reserve_color_vertex_program_transform_param_default_uniform_data(
		sizeof(gProjection) / sizeof(float), (float *)&gProjection);

	//glUniformMatrix4fv(program->uloc_project, 1, GL_FALSE, mat_project);

	//glUniform4f(program->uloc_primcol, mPrimitiveColour.GetRf(), mPrimitiveColour.GetGf(), mPrimitiveColour.GetBf(), mPrimitiveColour.GetAf());
	//glUniform4f(program->uloc_envcol,  mEnvColour.GetRf(),       mEnvColour.GetGf(),       mEnvColour.GetBf(),       mEnvColour.GetAf());
	//glUniform1f(program->uloc_primlodfrac, mPrimLODFraction);
	//extern u32 gRDPFrame;
	//glUniform1i(program->uloc_foo, gRDPFrame);
}

static inline void ToGxmVertex(struct color_vertex *out, const DaedalusVtx *in)
{
	out->position.x = in->Position.x;
	out->position.y = in->Position.y;
	out->position.z = in->Position.z;
	out->color = in->Colour.GetColour();
}

void RendererVita::RenderTriangles(DaedalusVtx * p_vertices, u32 num_vertices, bool disable_zbuffer)
{
	//LOG("RendererVita::RenderTriangles\n");

	PrepareRenderState(gProjection.m, disable_zbuffer);

#if 0
	#define MIN(a, b) ((a) < (b) ? (a) : (b))

	struct color_vertex vertices[300];
	while (num_vertices > 0) {
		uint32_t num = MIN(num_vertices, ARRAY_SIZE(vertices));
		for (uint32_t i = 0; i < num; i++)
			ToGxmVertex(&vertices[i], &p_vertices[i]);

		gxm_vertex_buffer_color_push(SCE_GXM_PRIMITIVE_TRIANGLES, vertices, num);

		num_vertices -= num;
		p_vertices += num;
	}
#endif

	for (u32 i = 0; i < num_vertices; i += 3) {
		struct color_vertex vertices[3];
		for (u32 j = 0; j < ARRAY_SIZE(vertices); j++) {
			ToGxmVertex(&vertices[j], &p_vertices[i + j]);
			//vertices[j].color = 0xFF0000FF;
			//vertices[j].position.z = 0.0f;

			vector3f before = vertices[j].position;
			vector3f after;
			vector3f_matrix4x4_mult(&after, (float (*)[4])&gProjection,
				&before, 1.0f);

			//LOG("ORIG  {%f, %f, %f}\n", before.x, before.y, before.z);
			//LOG("AFTER {%f, %f, %f}\n", after.x, after.y, after.z);
			//LOG("\n");
		}

		gxm_vertex_buffer_color_push(SCE_GXM_PRIMITIVE_TRIANGLES, vertices, ARRAY_SIZE(vertices));
	}
}

void RendererVita::TexRect(u32 tile_idx, const v2 & xy0, const v2 & xy1, TexCoord st0, TexCoord st1)
{

}

void RendererVita::TexRectFlip(u32 tile_idx, const v2 & xy0, const v2 & xy1, TexCoord st0, TexCoord st1)
{
}

void RendererVita::FillRect(const v2 & xy0, const v2 & xy1, u32 color)
{
}

void RendererVita::Draw2DTexture(f32 x0, f32 y0, f32 x1, f32 y1,
								f32 u0, f32 v0, f32 u1, f32 v1,
								const CNativeTexture * texture)
{
}

void RendererVita::Draw2DTextureR(f32 x0, f32 y0, f32 x1, f32 y1,
								 f32 x2, f32 y2, f32 x3, f32 y3,
								 f32 s, f32 t)
{
}

bool CreateRenderer()
{
	gRendererVita = new RendererVita();
	gRenderer    = gRendererVita;
	return true;
}

void DestroyRenderer()
{
	delete gRendererVita;
	gRendererVita = nullptr;
	gRenderer    = nullptr;
}
