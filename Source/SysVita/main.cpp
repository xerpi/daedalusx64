#include <stdlib.h>
#include <stdio.h>

#include <psp2/ctrl.h>
#include <psp2/kernel/processmgr.h>

#include "BuildOptions.h"
#include "Config/ConfigOptions.h"
#include "Core/Cheats.h"
#include "Core/CPU.h"
#include "Core/CPU.h"
#include "Core/Memory.h"
#include "Core/PIF.h"
#include "Core/RomSettings.h"
#include "Core/Save.h"
#include "Debug/DBGConsole.h"
#include "Debug/DebugLog.h"
#include "Graphics/GraphicsContext.h"
#include "HLEGraphics/TextureCache.h"
#include "Input/InputManager.h"
#include "Interface/RomDB.h"
#include "System/Paths.h"
#include "System/System.h"
#include "Test/BatchTest.h"
#include "Utility/IO.h"
#include "Utility/Preferences.h"
#include "Utility/Profiler.h"
#include "Utility/Thread.h"
#include "Utility/Translate.h"
#include "Utility/Timer.h"

#include "debugScreen.h"
#include "netlog.h"

#define DAEDALUS_VITA_PATH(p) "ux0:/DaedalusX64/" p
#define LOAD_ROM              DAEDALUS_VITA_PATH("Roms/Super Mario 64 (USA).n64")
//#define LOAD_ROM              DAEDALUS_VITA_PATH("Roms/rdpdemo.z64")

#define NETLOG_IP "192.168.1.133"
#define NETLOG_PORT 3490

#define LOG(...) \
	do { \
		netlogf(__VA_ARGS__); \
		psvDebugScreenPrintf(__VA_ARGS__); \
	} while (0)

extern "C" {

extern void __sinit(struct _reent *);

int _newlib_heap_size_user = 96 * 1024 * 1024;

int chdir(const char *path) {return 0;}
int mkdir(const char *path) {return 0;}
int chmod(const char *path, mode_t mode) {return 0;}
long pathconf(const char *path, int name) {return 0;}

char *getcwd(char *buf, size_t size)
{
	strlcpy(buf, "ux0:/", size);
	return buf;
}

unsigned int sleep(unsigned int seconds)
{
	sceKernelDelayThreadCB(seconds * 1000 * 1000);
	return 0;
}

int usleep(useconds_t usec)
{
	sceKernelDelayThreadCB(usec);
	return 0;
}

// https://github.com/vitasdk/newlib/issues/11
__attribute__((constructor(101))) void pthread_setup(void)
{
	pthread_init();
	__sinit(_REENT);
}

void __assert_func(const char *filename, int line, const char *assert_func, const char *expr)
{
	LOG("%s:%d:4: error: assertion \"%s\" failed in function %s\n",
		filename, line, expr, assert_func );
	abort();
}

void abort(void)
{
	LOG("Abort called\n");
	sceKernelExitProcess(0);
	while (1) sleep(1);
}

}

static void wait_press()
{
	SceCtrlData pad;
	memset(&pad, 0, sizeof(pad));
	while (1) {
		sceCtrlPeekBufferPositive(0, &pad, 1);

		if (pad.buttons & SCE_CTRL_CROSS)
			break;
	}
}

static void Initialize()
{
	strcpy(gDaedalusExePath, DAEDALUS_VITA_PATH(""));
	strcpy(g_DaedalusConfig.mSaveDir, DAEDALUS_VITA_PATH("SaveGames/"));

	bool ret = System_Init();
	LOG("System_Init(): %d\n", ret);
}

void HandleEndOfFrame()
{
	SceCtrlData pad;
	sceCtrlPeekBufferPositive(0, &pad, 1);

	if (pad.buttons & SCE_CTRL_CROSS)
		CPU_Halt("Exiting");
}

int main(int argc, char* argv[])
{
	netlog_init(NETLOG_IP, NETLOG_PORT);
	psvDebugScreenInit();

	LOG("DaedalusX64 - Vita port\n");

	Initialize();
	System_Open(LOAD_ROM);
	CPU_Run();
	System_Close();
	System_Finalize();

	LOG("\nPress X to exit\n");
	wait_press();

	LOG("\nExiting...\n");

	netlog_fini();
	return 0;
}
