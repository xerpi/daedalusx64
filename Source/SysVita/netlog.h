#ifndef NETLOG_H
#define NETLOG_H

#include <stdarg.h>

#ifdef __cplusplus
extern "C" {
#endif

void netlog_init(const char *ip, unsigned short int port);
void netlog_fini(void);
void netlog(const char *s);
void netlogv(const char *format, va_list ap);
void netlogf(const char *format, ...) __attribute__((format(printf, 1, 2)));

#define LOG(...) netlogf(__VA_ARGS__)

#ifdef __cplusplus
}
#endif

#endif
