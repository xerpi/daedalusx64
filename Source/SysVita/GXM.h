#ifndef SYSVITA_GXM_H_
#define SYSVITA_GXM_H_

#include <psp2/gxm.h>

#include "Utility/DaedalusTypes.h"

// FIXME: burn all of this with fire.

static inline void sceGuFog(float mn, float mx, u32 col) {}

enum EGuTextureWrapMode
{
	GU_CLAMP			= 0, //GXM_CLAMP_TO_EDGE,
	GU_REPEAT			= 1  //GXM_REPEAT,
};

enum EGuMatrixType
{
	GU_PROJECTION		= 0//GXM_PROJECTION,
};

struct ScePspFMatrix4
{
	float m[16];
};

void sceGuSetMatrix(EGuMatrixType type, const ScePspFMatrix4 * mtx);

#endif // SYSVITA_GXM_H_
