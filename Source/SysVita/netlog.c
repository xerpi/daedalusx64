#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <psp2/sysmodule.h>
#include <psp2/net/net.h>
#include "netlog.h"

#define NET_INIT_SIZE (64 * 1024)

static void *net_memory = NULL;
static SceNetSockaddrIn host_addr;
static int udp_sock;

void netlog_init(const char *ip, unsigned short int port)
{
	if (sceSysmoduleIsLoaded(SCE_SYSMODULE_NET) != SCE_SYSMODULE_LOADED)
		sceSysmoduleLoadModule(SCE_SYSMODULE_NET);

	if (sceNetShowNetstat() == SCE_NET_ERROR_ENOTINIT) {
		SceNetInitParam init_param;

		net_memory = malloc(NET_INIT_SIZE);
		if (!net_memory)
			return;

		init_param.memory = net_memory;
		init_param.size = NET_INIT_SIZE;
		init_param.flags = 0;
		sceNetInit(&init_param);
	}

	udp_sock = sceNetSocket("netlog", SCE_NET_AF_INET, SCE_NET_SOCK_DGRAM, 0);

	sceNetInetPton(SCE_NET_AF_INET, ip, &host_addr.sin_addr);
	host_addr.sin_family = SCE_NET_AF_INET;
	host_addr.sin_port = sceNetHtons(port);
}

void netlog_fini(void)
{
	sceNetSocketClose(udp_sock);

	//sceNetTerm();

	//if (sceSysmoduleIsLoaded(SCE_SYSMODULE_NET) == SCE_SYSMODULE_LOADED)
	//	sceSysmoduleUnloadModule(SCE_SYSMODULE_NET);

	if (net_memory) {
		free(net_memory);
		net_memory = NULL;
	}
}

static inline void udp_send(const char *buffer, int size)
{
	sceNetSendto(udp_sock, buffer, size, SCE_NET_MSG_DONTWAIT,
		(SceNetSockaddr *)&host_addr, sizeof(host_addr));
}

void netlog(const char *s)
{
	udp_send(s, strlen(s));
}

void netlogv(const char *format, va_list ap)
{
	char buf[4 * 1024];
	int ret = vsnprintf(buf, sizeof(buf), format, ap);
	netlog(buf);
}

void netlogf(const char *format, ...)
{
	char buffer[4 * 1024];
	va_list args;

	va_start(args, format);
	int size = vsnprintf(buffer, sizeof(buffer), format, args);
	udp_send(buffer, size);
	va_end(args);
}
